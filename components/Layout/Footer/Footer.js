import "./footer.module.scss";
import Image from "next/image";
import Grid from "@mui/material/Grid";
import { Text } from "@/ui/index";
import Link from "next/link";
import FooterMenu from "./FooterMenu";
import Container from "@mui/material/Container";
import styles from "./footer.module.scss";
import Fb from "assets/images/Fb.png";
import Linkedin from "assets/images/Linkedin.png";
import Twitter from "assets/images/Twitter.png";
import Youtube from "assets/images/Youtube.png";
import Instagram from "assets/images/Instagram.png";
import BgFooter from "assets/images/footer-bg.png";

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles["footer-triangle"]} />
            <div className={styles["footer-bg"]}>
                <Image src={BgFooter} />
            </div>
            <Container className="to-top" maxWidth="xl">
                <Grid container spacing={2} className={styles.content}>
                    <Grid item md={5}>
                        <Text variant="title-section" color="white">
                            #Let’s Make Indonesia Games Great!
                        </Text>
                        <Text variant="title-tag" color="white" className="font-general">
                            Contact: hello@majamojo.id
                        </Text>
                    </Grid>
                    <Grid item md={7} className={styles.pad}>
                        <ul className="d-block">
                            {/* <li>
                                <Link href="#"><a>Our Products</a></Link>
                            </li>
                            <li>
                                <Link href="#"><a>About Us</a></Link>
                            </li>
                            <li>
                                <Link href="#"><a>Careers</a></Link>
                            </li>
                            <li>
                                <Link href="#"><a>Services</a></Link>
                            </li> */}
                            <FooterMenu />
                        </ul>
                        <br />
                        <ul className="d-block">
                            <li>
                                <Link href="#">
                                    <a>
                                        <Image src={Fb}></Image>
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        <Image src={Linkedin} />
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        <Image src={Twitter} />
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        <Image src={Youtube} />
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>
                                        <Image src={Instagram} />
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </Grid>
                    <Grid item md={12} className={styles.w100}>
                        <div className={styles["footer-bottom"]}>
                            <div className="float-right">
                                <ul>
                                    <li>
                                        <Link href="#"><a>Terms & Conditions</a></Link>
                                    </li>
                                    <li>
                                        <Link href="#"><a>Privacy Policy</a></Link>
                                    </li>
                                </ul>
                            </div>

                            <div className="float-left">
                                <span className="font-bold">
                                    &copy; 2022 Majamojo All Rights Reserved
                                </span>
                            </div>

                            <div className="clearfix"></div>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </footer>
    );
};

export default Footer;
