import data from "@/section/KnowUs/data";
import Link from "next/link";
import styles from "./Header.module.scss";
import { useRouter } from "next/router";

const HeaderMenu = () => {
    const router = useRouter();

    let data = [
        { link: '/', title: 'Home' },
        { link: '/our-products', title: 'Our Products' },
        { link: '/news', title: 'News' },
        { link: '/about-us', title: 'About Us' },
        { link: '/career', title: 'Career' },
    ];

    const handleActiveLink = (link) => {
        if (link !== '/') return router.pathname.includes(link.split('-')[0]);

        if (router.pathname == '/') return true;

        return false;
    }

    return (
        <ul>
            {data.map((list, idx) => (
                <li key={idx} className={handleActiveLink(list.link) ? styles.active : undefined}>
                    <Link href={list.link}><a>{list.title}</a></Link>
                </li>
            ))}
        </ul>
    )
}

export default HeaderMenu