import { useState } from "react";
import styles from "./Header.module.scss";
import { Fragment } from "react";
import { Container, Drawer, Box } from "@mui/material";
import Logo from "assets/images/logo.png";
import Image from "next/image";
import Link from "next/link";
import HeaderMenu from "./HeaderMenu";

const Header = () => {
    const [isDrawerOpen, setIsDrawerOpen] = useState(false);
    const barFunction = () => {
        setIsDrawerOpen(true);
    };
    const closeBar = () => {
        setIsDrawerOpen(false);
    };
    return (
        <Fragment>
            <div className={styles.menu}>
                <Container maxWidth="xl">
                    <div className={`${styles["menu__logo"]} float-left`}>
                        <Link href='/'>
                            <Image src={Logo} alt="Logo" title="logo" />
                        </Link>
                    </div>
                    <div className="float-right">
                        <div className={styles.bar} onClick={barFunction}>
                            <div className={styles["bar-bar"]}></div>
                            <div className={styles["bar-bar"]}></div>
                            <div className={styles["bar-bar"]}></div>
                        </div>

                        {/* ketika mobile */}
                        <Drawer
                            anchor="left"
                            open={isDrawerOpen}
                            onClose={closeBar}
                        >
                            <Box
                                p={2}
                                width="300px"
                                role="presentation"
                            >
                                <div className={`${styles['menu__mobile']}`}>
                                    <HeaderMenu />
                                    <div className={styles.bottom}>&copy; 2022 Majamojo All Rights Reserved</div>
                                </div>
                            </Box>
                        </Drawer>

                        {/* ketika desktop */}
                        <HeaderMenu />
                    </div>
                    <div className="clearfix" />
                </Container>
            </div>
        </Fragment>
    );
};

export default Header;
