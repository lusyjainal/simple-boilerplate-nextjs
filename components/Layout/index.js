import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Head from 'next/head'

const Index = ({ meta, children }) => {
  return (
    <>
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <div>{children}</div>
      <Footer />
    </>
  );
}

export default Index;
