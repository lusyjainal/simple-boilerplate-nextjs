import styles from "./CardArticle.module.scss";
import { Stack, Skeleton } from "@mui/material";
import { Text, Card } from "components/UI";
import ButtonLabel from "../ButtonLabel";
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

const Cardarticle = ({
    label,
    title,
    description,
    linkImage,
    styleLabel = "tags-primary",
    slug,
    isLoading
}) => {

    const router = useRouter();

    const handleMove = () => {
        router.push(`/news/${slug}`)
    }

    return (
        <>
            {(() => {
                if (isLoading) return <Skeleton className={styles['card-loading']} variant="rectangular" />;

                return <Card className={styles["card-new"]} onClick={handleMove}>
                    <div className={styles["bg-opacity"]}></div>
                    <div className={styles["card-img"]}>
                        <img src={linkImage} alt={title} title={title} />
                    </div>
                    <Stack direction="row" spacing={1} className={styles.tags}>
                        <ButtonLabel
                            label={label}
                            color="primary"
                            styleLabel={styleLabel}
                        />
                    </Stack>
                    <div className={styles.content}>
                        <Text className={`${styles["content__title"]} color-white`}>
                            {title}
                        </Text>
                        <Text variant="title-tag" className={styles["content__desc"]}>
                            {description}
                        </Text>
                    </div>
                </Card>

            })()}
        </>
    );
};


Cardarticle.propTypes = {
    /**
     * label digunakan untuk kategori
     */
    label: PropTypes.string,
    /**
     * title digunakan untuk judul
     */
    title: PropTypes.string.isRequired,
    /**
    * description digunakan untuk detail
    */
    description: PropTypes.string,
    /**
     * linkImage digunakan untuk url gambar
     */
    linkImage: PropTypes.string,
    /**
     * styleLabel digunakan untuk style kategori
     */
    styleLabel: PropTypes.oneOf(['tags-primary', 'tags-secondary']),
    /**
    * slug digunakan untuk link ke detal Card
    */
    slug: PropTypes.string,
    /**
    * isLoading digunakan untuk data masih dalam proses
    */
   isLoading: PropTypes.bool
};

export default Cardarticle;
