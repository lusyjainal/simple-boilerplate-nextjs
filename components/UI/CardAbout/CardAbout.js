import { Container, Grid } from "@mui/material";
import styles from './CardAbout.module.scss';
import { Text, Card } from 'components/UI';
import ArrowRight from 'assets/images/arrow-right.png'
import Image from "next/image";

const Cardabout = ({
    title,
    description,
    linkImage
}) => {
    return (
        <Card className={styles.wrap}>
            <div className={styles['bg-help']}></div>
            <div className={styles['about__img']}>
                <img src={linkImage} alt={title} title={title} />
            </div>
            <div className={styles.content}>
                <Text variant='title-section' color='white'>{title}</Text>
                <Grid container spacing={0}>
                    <Grid item md={11}>
                        <Text color="white">{description}</Text>
                    </Grid>
                    <Grid item md={1}>
                        <div className={styles.pt16}>
                            <Image src={ArrowRight} title="Arrow" alt="Arrow" />
                        </div>
                    </Grid>
                </Grid>
            </div>
        </Card>
    );
}

export default Cardabout;