import React from 'react';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm'
import PropTypes from 'prop-types';

const MarkdownText = (props) => {

    const { markdown } = props;

    return (
        <ReactMarkdown children={markdown} remarkPlugins={[remarkGfm]} {...props} />
    );
}


MarkdownText.propTypes = {
    /**
     * type markdown adalah sebuah balikan data dari strapi seperti descripton
     */
    markdown: PropTypes.string.isRequired,

};

MarkdownText.defaultProps = {
    markdown: '',
};

export default MarkdownText;
