import PropTypes from 'prop-types';
import { StyledH1, StyledH2, StyledH3, StyledH4, StyledP } from './Styled';

const Text = ({ children, variant, color, size, className, ...props }) => {
  return (
    <>
      {(() => {
        if (variant == 'title-hero') return <StyledH1 color={color} className={className} size={size} {...props}>{children}</StyledH1>

        if (variant == 'title-section') return <StyledH2 color={color} className={className} size={size} {...props}>{children}</StyledH2>

        if (variant == 'title-tag') return <StyledH3 color={color} className={className} size={size} {...props}>{children}</StyledH3>

        if (variant == 'title-card') return <StyledH4 color={color} className={className} size={size} {...props}>{children}</StyledH4>

        return <StyledP color={color} className={className} {...props} size={size}>{children}</StyledP>

      })()}
    </>
  );
}

export default Text;

Text.propTypes = {
  /**
   * children digunakan untuk text yang akan dimasukan
   */
  children: PropTypes.string.isRequired,
  /**
   * digunakan untuk salah satu jenis font
   */
  variant: PropTypes.oneOf(['title-hero', 'title-section', 'title-tag', 'title-card', '']),
  /**
   * digunakan untuk memberikan warna (#fff)
   */
  color: PropTypes.string,
  /**
   * digunakan untuk memberikan ukuran (16)
   */
  size: PropTypes.number,
};

Text.defaultProps = {
  variant: '',
};
