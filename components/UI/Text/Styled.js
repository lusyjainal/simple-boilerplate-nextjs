import Styled from '@emotion/styled';
import { FONT_TITLE, FONT_COLOR, SCREEN_MOBILE } from 'constants/variable';

export const StyledH1 = Styled.h1`
    font-size: 50px;
    font-family: ${FONT_TITLE};
    color: ${({ color }) => FONT_COLOR(color)};
    @media (${SCREEN_MOBILE}) {
        font-size: 32px;
    }
`;

export const StyledH2 = Styled.h2`
    font-size: 36px;
    margin-top: 50px;
    margin-bottom: 40px; 
    font-family: ${FONT_TITLE};
    color: ${({ color }) => FONT_COLOR(color)};
    @media (${SCREEN_MOBILE}) {
        font-size: 24px;
    }
`;

export const StyledH3 = Styled.h3`
    font-size: 30px;
    font-family: ${FONT_TITLE};
    color: ${({ color }) => FONT_COLOR(color)};
    @media (${SCREEN_MOBILE}) {
        font-size: 21px;
    }
`;

export const StyledH4 = Styled.h4`
    font-size: 24px;
    font-family: ${FONT_TITLE};
    color: ${({ color }) => FONT_COLOR(color)};
    @media (${SCREEN_MOBILE}) {
        font-size: 18px;
    }
`;

export const StyledP = Styled.p`
    font-family: Poppins;
    font-size: ${({ size }) => size ? `${size}px` : '16px'};
    color: ${({ color }) => FONT_COLOR(color)};
    @media (${SCREEN_MOBILE}) {
        font-size: 14px;
    }
`;
