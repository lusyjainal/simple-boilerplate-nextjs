import './button.module.scss';
import styles from './button.module.scss'

const Button = ({ children, type }) => {
  return (
    <button
      type={type || 'button'}
      className= {styles.button}
    >
      { children }
    </button>
  );
}

export default Button;
