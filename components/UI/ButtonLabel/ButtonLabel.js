import { Chip } from '@mui/material';
import styles from './Button.module.scss';

const Buttonlabel = ({
    label,
    variant,
    color,
    styleLabel,
    ...props
}) => {
    return (
        <Chip
            label={label}
            variant={variant}
            color={color}
            className={`${styles[styleLabel]}`}
            style={{
                fontWeight: 'bold'
            }}
            {...props}
        />
    );
}

export default Buttonlabel;
