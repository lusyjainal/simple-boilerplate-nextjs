export { default as HeroTopSlider } from './HeroTopSlider';
export { default as WhatsNew } from './WhatsNew';
export { default as HeroBottom } from './HeroBottom';
export { default as KnowUs } from './KnowUs';
export { default as AboutDescription } from './AboutDescription'; 
export { default as OurValue } from './OurValue'; 
export { default as AboutUsHero } from './AboutUsHero'; 