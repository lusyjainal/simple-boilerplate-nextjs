import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import data from "./data";
import { Text, ButtonLabel, Card } from "@/ui/index";
import { Container } from "@mui/material";
import styles from "./HeroTopSlider.module.scss";

const HerotopSlider = () => {
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [index, setIndex] = useState(0);

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        autoplaySpeed: 2000,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        pauseOnFocus: false,
        fade: true,
        customPaging: (idx) => setIndex(idx)
    };

    return (
        <>
            <div className="position-relative">
                <Slider
                    asNavFor={nav2}
                    ref={(slider1) => setNav1(slider1)}
                    {...settings}
                >
                    {data.map((list, idx) => (
                        <div key={idx}>
                            <div
                                style={{
                                    backgroundImage: `url(${list.imgBig})`,
                                    backgroundSize: "cover",
                                    height: 850,
                                    backgroundPositionY: "bottom",
                                }}
                            >
                                <Container
                                    maxWidth="xl"
                                    className={styles["herro-top"]}
                                >
                                    <Text variant="title-tag" color="white">
                                        {list.title}
                                    </Text>
                                    <Text variant="title-hero" color="white" style={{
                                        width: '65%'
                                    }}>
                                        {list.description}
                                    </Text>
                                    <a href="#" className={styles.href}>
                                        <ButtonLabel
                                            variant="primary"
                                            label="See Our Story"
                                            className={styles.button}
                                        />
                                    </a>
                                </Container>
                            </div>
                        </div>
                    ))}
                </Slider>
                <Container maxWidth="xl" className={styles['slider-nav-2']}>
                    <Slider
                        className={styles['slider-nav-2_slider']}
                        asNavFor={nav1}
                        ref={(slider2) => setNav2(slider2)}
                        slidesToShow={4}
                        swipeToSlide={true}
                        focusOnSelect={true}
                        arrows={false}
                    >
                        {data.map(({ smallCard }, idx, jajal, ok) => (
                            <div key={idx}>
                                <Card
                                    className={`${styles["card-hero"]} card-hero-active`}
                                >
                                    <div className={styles['card-hero__img']}>
                                        <img src={smallCard.img} />
                                    </div>
                                    <div className={styles["card-hero__wrap-text"]}>
                                        <Text
                                            variant="title-card"
                                            color="white"
                                            style={{ marginTop: 0 }}
                                        >
                                            {smallCard.label}
                                        </Text>
                                        <Text color="white">
                                            {smallCard.description}
                                        </Text>
                                    </div>
                                </Card>
                            </div>
                        ))}
                    </Slider>
                </Container>
            </div>
        </>
    );
};

export default HerotopSlider;
