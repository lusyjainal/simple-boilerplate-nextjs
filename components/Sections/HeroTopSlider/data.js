const hero = [
    {   
        smallCard:{
            label: "Majamojo",
            img: "https://awsimages.detik.net.id/community/media/visual/2020/07/13/manga-naruto-1_43.webp?w=700&q=90",
            description: "Majamojo is getting the best game publisher across SEA"
        },
        title: "#RoadToSea",
        description: "Majamojo is getting the best game publisher across SEA",
        imgBig: "https://i.ibb.co/5G7vchP/Rectangle-11.png",
    },
    {   
        smallCard:{
            label: "Product",
            img: "https://awsimages.detik.net.id/community/media/visual/2020/07/13/manga-naruto-1_43.webp?w=700&q=90",
            description: "Fall Guy v4.5 : Introducing Ultimate Showdown Battle"
        },
        title: "Fall guy",
        description: "Fall Guy Ultimate Showdown has been released Today",
        imgBig: "https://i.ibb.co/s2zG8yy/slider.png",
    },
    {
        smallCard:{
            label: "Product",
            img: "https://awsimages.detik.net.id/community/media/visual/2020/07/13/manga-naruto-1_43.webp?w=700&q=90",
            description: "Katamari Damashi : Is it going to win game of the year this time? "
        },
        title: "Game Dev",
        description: "Indonesian Community Supports Local Game Creator",
        imgBig: "https://i.ibb.co/s2zG8yy/slider.png",
    },
    {
        smallCard:{
            label: "News",
            img: "https://awsimages.detik.net.id/community/media/visual/2020/07/13/manga-naruto-1_43.webp?w=700&q=90",
            description: "NamcoBandai is giving a clue of Persona 5 Steam Version "
        },
        title: "Game Dev",
        description: "Indonesian Community Supports Local Game Creator",
        imgBig: "https://i.ibb.co/s2zG8yy/slider.png",
    },
];

export default hero;
