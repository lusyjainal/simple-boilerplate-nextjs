import { Container } from "@mui/material";
import { Text } from '@/ui/index';
import KnowUs from "./KnowUs";

const KnowUsContainer = () => {
    return (
        <Container maxWidth="xl">
            <Text variant='title-section' color="blue">Know Us Better</Text>
            <KnowUs />
        </Container>
    )
}

export default KnowUsContainer;