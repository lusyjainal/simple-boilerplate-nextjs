import { Grid } from "@mui/material";
import { CardAbout } from '@/ui/index';
import data from './data';

const KnowUs = () => {
    return (
        <Grid container spacing={2}>
            {data.map((list, idx) => (
                <Grid item xs={12} md={6} key={idx}>
                    <CardAbout
                        title={list.title}
                        description={list.description}
                        linkImage={list.linkImage}
                    />
                </Grid>
            ))}
        </Grid>
    )
}

export default KnowUs