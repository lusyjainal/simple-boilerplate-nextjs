import React, { createRef } from 'react';
import { Container, Grid, IconButton } from '@mui/material';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import styles from './HerroBottom.module.scss';
import ArrowCircleLeftOutlinedIcon from '@mui/icons-material/ArrowCircleLeftOutlined';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { Text } from '@/ui/index';
import HerroBottomSlide from 'assets/images/herro-bottom.png';
import Image from "next/image";

const Herrobottom = () => {
    
    //creating the ref
    const customeSlider = createRef();

    const gotoNext = () => {
        customeSlider.current.slickNext()
    }

    const gotoPrev = () => {
        customeSlider.current.slickPrev()
    }

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true
    };

    return (
        <div
            style={{
                backgroundImage: `url(https://i.ibb.co/SrtZ488/bg-herro-bottom.png)`,
                height: 580,
                backgroundSize: 'cover',
                position: 'relative'
            }}
            className={styles['herro-bottom']}
        >
            <Slider ref={customeSlider} {...settings}>
                <div key={1}>
                    <Container maxWidth="xl">
                        <Grid container className={styles['herro-bottom__wrap']}>
                            <Grid item md={5} className={styles['herro-bottom__wrap__left']}>
                                <Text variant="title-section" color="white"> 1 Our Products</Text>
                                <img src="https://i.ibb.co/4Zz3JCG/title-herro-bottom.png" alt="title-herro-bottom" border="0" />
                                <Text color="white">
                                    Fall Guys: Ultimate Knockout is a platform battle royale game developed by Mediatonic and published by Devolver Digital. It was released for Microsoft Windows and PlayStation 4 on 4 August 2020.
                                </Text>
                            </Grid>
                            <img className={styles['herro-bottom__wrap__img-right']} src="https://i.ibb.co/990khvt/herro-bottom.png" alt="herro-bottom" border="0" />
                        </Grid>
                    </Container>
                </div>
                <div key={2}>
                    <Container maxWidth="xl">
                        <Grid container className={styles['herro-bottom__wrap']}>
                            <Grid item md={5} className={styles['herro-bottom__wrap__left']}>
                                <Text variant="title-section" color="white">2 Our Products</Text>
                                <img src="https://i.ibb.co/4Zz3JCG/title-herro-bottom.png" alt="title-herro-bottom" border="0" />
                                <Text color="white">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                </Text>
                            </Grid>
                            <img className={styles['herro-bottom__wrap__img-right']} src="https://i.ibb.co/990khvt/herro-bottom.png" alt="herro-bottom" border="0" />
                        </Grid>
                    </Container>
                </div>
            </Slider>

            <div className={styles['btn-arrows']}>
                <IconButton className={styles['btn-arrows__icon']} onClick={gotoPrev} color="secondary" aria-label="add to shopping cart">
                    <ArrowCircleLeftOutlinedIcon fontSize="inherit" />
                </IconButton>
                <IconButton className={styles['btn-arrows__icon']} onClick={gotoNext} color="secondary" aria-label="add to shopping cart">
                    <ArrowCircleRightOutlinedIcon fontSize="inherit" />
                </IconButton>
            </div>
        </div>
    );
}

export default Herrobottom;
