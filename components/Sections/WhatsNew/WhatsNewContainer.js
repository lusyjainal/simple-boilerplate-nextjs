import { Text } from '@/ui/index';
import { Container } from '@mui/material';
import { ButtonLabel } from '@/ui/index';
import styles from './WhatsNew.module.scss'
import Whatsnew from './WhatsNew';
import { BASE_URL } from 'constants/services';
import useSWR from 'swr'; // dari next.js
import fetcher from 'hooks/fetcher';

const Whatsnewcontainer = () => {
    const { data, error } = useSWR(`${BASE_URL}/api/blogs?populate=*&pagination[pageSize]=5&sort[0]=id:desc`, fetcher);

    return (
        <>
            <Container maxWidth="xl">
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}>
                    <Text variant='title-section' color="blue">What’s New</Text>
                    <ButtonLabel
                        className={styles.button}
                        label="See All News"
                        variant="outlined" color="primary"
                    />
                </div>
                <Whatsnew blog={data} isLoading={!data} isError={error} />
            </Container>
        </>
    );
}

export default Whatsnewcontainer;
