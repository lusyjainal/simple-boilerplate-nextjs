import React from 'react';
import { Grid } from '@mui/material';
import { CardArticle } from '@/ui/index';
import { BASE_URL } from 'constants/services';

const Whatsnew = (props) => {
    const { blog, isLoading, isError } = props; // es 6

    const handleDesc = (text, maxLength) => {
        if (text.length > maxLength) return `${text.substr(0, maxLength)}...`;
        return text;
    }

    return (
        <Grid container columnSpacing={3} rowSpacing={2}>
            {(() => {
                if (isError) return <h1>Server Error</h1>;
                
                if (isLoading) {
                    return (
                        <>
                            {[1, 2].map((idx) => (
                                <Grid item md={6} xs={12} key={idx}>
                                    <CardArticle isLoading={true} />
                                </Grid>
                            ))}
                            {[1, 2, 3].map((idx) => (
                                <Grid item md={4} xs={12} key={idx}>
                                    <CardArticle isLoading={true} />
                                </Grid>
                            ))}
                        </>
                    )
                }

                const { data } = blog;

                if (data) {
                    return (
                        <>
                            {data.map((list, idx) => idx <= 1 && (
                                <Grid item md={6} xs={12} key={idx}>
                                    <CardArticle
                                        label={list.attributes.category.data?.attributes.title}
                                        title={list.attributes.title}
                                        description={handleDesc(list.attributes.headline, 50)}
                                        linkImage={`${BASE_URL}${list.attributes.image.data.attributes.url}`}
                                        styleLabel={list.attributes.category.data?.attributes.title === 'News' ? 'tags-primary' : 'tags-secondary'}
                                        slug={list.attributes.slug}
                                    />
                                </Grid>
                            )
                            )}
                            {data.map((list, idx) => idx >= 2 && (
                                <Grid item md={4} xs={12} key={idx}>
                                    <CardArticle
                                        label={list.attributes.category.data?.attributes.title}
                                        title={list.attributes.title}
                                        description={handleDesc(list.attributes.headline, 35)}
                                        linkImage={`${BASE_URL}${list.attributes.image.data.attributes.url}`}
                                        styleLabel={list.attributes.category.data?.attributes.title === 'News' ? 'tags-primary' : 'tags-secondary'}
                                        slug={list.attributes.slug}
                                    />
                                </Grid>
                            )
                            )}
                        </>
                    )
                }

            })()}
        </Grid>
    );
}

export default Whatsnew;
