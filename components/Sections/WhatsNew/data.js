const data = [
  {
    label: "News",
    title: "Majamojo",
    description: "Majamojo is getting the best game publisher across SEA",
    linkImage: "https://www.dailysia.com/wp-content/uploads/2022/02/Sasuke-Uchiha_1.jpg",
    longDescription:"lorem ipsum"
  },
  {
    label: "Product",
    title: "Fall guy",
    description: "Fall Guy Ultimate Showdown has been released Today",
    linkImage: "https://www.dailysia.com/wp-content/uploads/2022/02/Sasuke-Uchiha_1.jpg",
    longDescription:"lorem ipsum"
  },
  {
    label: "News",
    title: "Game Dev",
    description: "Indonesian Community Supports Local Game Creator",
    linkImage: "https://www.dailysia.com/wp-content/uploads/2022/02/Sasuke-Uchiha_1.jpg",
    longDescription:"lorem ipsum"
  },
  {
    label: "Product",
    title: "Katamari",
    description: "Player Support Katamari Bug fix By submiting Reports",
    linkImage: "https://www.dailysia.com/wp-content/uploads/2022/02/Sasuke-Uchiha_1.jpg",
    longDescription:"lorem ipsum"
  },
  {
    label: "Product",
    title: "It Takes Two",
    description: "It Takes Two : Bringing A New Level For Harder Challenge",
    linkImage: "https://www.dailysia.com/wp-content/uploads/2022/02/Sasuke-Uchiha_1.jpg",
    longDescription:"lorem ipsum"
  },
];

export default data;