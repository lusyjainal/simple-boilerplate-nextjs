import { Text, MarkdownText } from '@/ui/index';
import { Container } from '@mui/material';
import styles from './AboutDescription.module.scss';
import markdown from './markdown';

const AboutDescription = () => {
    return (
        <Container className={styles['about-description']}>
            <Text variant='title-section' color="blue" className={styles['about-description__title']}>Our Story</Text>
            <MarkdownText markdown={markdown} className={styles['about-description__desc']} />
        </Container>
    );
}

export default AboutDescription;
