import React from "react";
import styles from "./AboutUsHero.module.scss";
import { Text } from "@/ui/index";
import { Container } from "@mui/material";
import ps5 from "assets/images/PS5.jpg";

const AboutUsHero = (props) => {
    return (
        <>
            <div className={styles.ceki}>
                <div className={styles.wrap}>
                    <div className={styles["bg-help"]} />
                    <Container max-width="xl">
                        <div className="to-top">
                            <div className={styles.padd}>
                                <Text variant="title-section" color="white">
                                    About Us
                                </Text>
                                <Text variant="title-hero" color="white">
                                    “It has been our mission to bring Indonesia Gaming
                                    Industries to worlwide audiences”
                                </Text>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        </>
    );
};

export default AboutUsHero;
