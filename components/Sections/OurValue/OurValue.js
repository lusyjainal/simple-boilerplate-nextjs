import Text from '@/ui/Text';
import { Grid } from '@mui/material';
import styles from './OurValue.module.scss';

const OurValue = () => {
    return (
        <div className={styles['our-value']}>
            <Text variant='title-section' color="blue" className={styles['our-value__title']}>Our Value</Text>
            <Grid container spacing={0} className={styles['our-value__wrap']}>
                <div className={styles.help}></div>
                <Grid item md={3} xs={12} className={styles['our-value__wrap-item']}>
                    <Text variant='title-section' color="white">Unlimited <br /> Creativity</Text>
                    <Text color="white">If you don’t want to manually deploy your site every time you add an article, you can create a build hook for your site</Text>
                </Grid>
                <Grid item md={3} xs={12} className={styles['our-value__wrap-item']}>
                    <Text variant='title-section' color="white">Unlimited <br /> Creativity</Text>
                    <Text color="white">If you don’t want to manually deploy your site every time you add an article, you can create a build hook for your site</Text>
                </Grid>
                <Grid item md={3} xs={12} className={styles['our-value__wrap-item']}>
                    <Text variant='title-section' color="white">Unlimited <br /> Creativity</Text>
                    <Text color="white">If you don’t want to manually deploy your site every time you add an article, you can create a build hook for your site</Text>
                </Grid>
                <Grid item md={3} xs={12} className={styles['our-value__wrap-item']}>
                    <Text variant='title-section' color="white">Unlimited <br /> Creativity</Text>
                    <Text color="white">If you don’t want to manually deploy your site every time you add an article, you can create a build hook for your site</Text>
                </Grid>
            </Grid>
        </div>
    );
}

export default OurValue;
