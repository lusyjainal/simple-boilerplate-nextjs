import React from 'react';

import MarkdownText from '../../../components/UI/MarkdownText/index';

export default {
  title: 'Components/UI/MarkdownText',
  component: MarkdownText,
};

const Template = (args) => <MarkdownText {...args} />;

const mardownExample = `
  Lorem markdownum arboribus has neve coniunx, ut nocent pectus, superata.
  Contendere **prioris** dona tantaeque, sed [terres gratia](http://imitatus.org/)
  et data aequo iaculum cupido et *Ceres*. [Vitreis dedit in](http://movet.io/)
  est *una ipsa* Lyaeumque eluserat cedere est. Anum
  [crista](http://membraprofundo.com/auxilium.aspx) indicat. Dies soror usus
  ponitque nostris.

  > Nostri aderat, nigra membra natusque posuitque nobiscum, quae. Quibus saevi
  > omni, iam *ulnis quid* excepto, pecudis ambiguus? Ensis ut cyclopum vacant
  > temptat, cecidere [cura](http://feraxque.com/vota). Est perfudit tribus
  > sinistra, facta fert tandem inmensa at Brotean deprensi mentis carbasa pronas
  > opem coniuge viri.
`;

export const TemplateText = Template.bind({});
TemplateText.args = {
  markdown: mardownExample,
};