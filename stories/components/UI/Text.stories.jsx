import React from 'react';

import Text  from '../../../components/UI/Text/index';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/UI/Text',
  component: Text,
};

const Template = (args) => <Text {...args} /> ;

export const TemplateText = Template.bind({});
TemplateText.args = {
  variant: '',
  children: 'majamojo',
};