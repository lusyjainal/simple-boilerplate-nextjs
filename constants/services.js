import { MODE_ENV } from './variable';
const MY_IP = '192.168.1.15';
// let BASE_URL = `http://${MY_IP}:1337`; // if error you use localhost
// let BASE_URL = `http://localhost:1337`; // local
let BASE_URL = `https://dev-majamojo-dtkzn.ondigitalocean.app`; // dev

switch (MODE_ENV) {
    case 'production':
        BASE_URL = 'https://api-majamojo-4tv3q.ondigitalocean.app';
        break;
    case 'staging':
        BASE_URL = 'https://staging.majamojo';
        break;
    case 'development':
        BASE_URL = 'https://dev-majamojo-dtkzn.ondigitalocean.app';
        break;
    default:
        BASE_URL = 'http://localhost:1337' // for local
}

export {
    BASE_URL
}