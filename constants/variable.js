export const MODE_ENV = 'development'; // development, staging, production
export const FONT_TITLE = 'Russo One';
export const FONT_COLOR = (color = 'default') => {
    if(color == 'blue') return '#0500F2';
    
    if(color == 'white') return '#FFFFFF';

    if(color == 'default') return '#000000de';

   return color;
};

export const SCREEN_DESKTOP = 'max-width: 1200px';
export const SCREEN_TABLET_MIN = 'min-width: 768px';
export const SCREEN_TABLET_MAX = 'min-width: 911px';
export const SCREEN_MOBILE = 'max-width: 767px';
export const SCREEN_MOBILE_XS = 'max-width: 320px';