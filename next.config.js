/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/about-us',
        destination: '/aboutus',
      },
      {
        source: '/our-products',
        destination: '/ourproducts',
      },
    ]
  },
}

module.exports = nextConfig
