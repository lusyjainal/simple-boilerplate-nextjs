import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { red, teal } from '@mui/material/colors';

const base = createTheme({
    palette: {
        primary: {
            main: '#0500F2',
            light: '#00A3FF',
            dark: '#000000'
        },
        secondary: {
            main: '#ffffff',
        },
        error: {
            main: red[400]
        },
        info: {
            main: teal[400]
        }
    },
    typography: {
        fontFamily: "Poppins",
        fontSize: 16
    },
});

const theme = responsiveFontSizes(base);

export default theme;