import React from 'react';
import News from 'views/News';

const NewsPage = () => {
    return (
        <div>
            <News />
        </div>
    );
}

export default NewsPage;