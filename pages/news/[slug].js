import React from 'react';
import DetailNews from 'views/details/DetailNews/DetailNews';
import { BASE_URL } from 'constants/services';

const Slug = ({ blogDetail }) => {
    return (
        <div>
            <DetailNews dataDetail={blogDetail} />
        </div>
    );
};

export async function getServerSideProps({ params }) {
    const { slug } = params;

    let resBlogDetail;
    try {

        const reqBlogDetail = await fetch(`${BASE_URL}/api/blogs?filters[slug][$eq]=${slug}&populate=*`);
        resBlogDetail = await reqBlogDetail.json();

    } catch (error) {
        console.error(error.message)
    }


    return {
        props: {
            blogDetail: resBlogDetail.data[0]
        }
    }

}

export default Slug;