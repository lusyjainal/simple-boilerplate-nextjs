import React from 'react';
import OurProducts from 'views/OurProducts';

const OurProductsPage = () => {
    return (
        <div>
            <OurProducts />
        </div>
    );
}

export default OurProductsPage;