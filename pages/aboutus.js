import React from 'react';
import AboutUs from 'views/AboutUs';

const AboutUsPage = () => {
    return (
        <div>
            <AboutUs />
        </div>
    );
}

export default AboutUsPage;