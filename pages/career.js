import React from 'react';
import Career from 'views/Career';

const CareerPage = () => {
    return (
        <div>
            <Career />
        </div>
    );
}

export default CareerPage;