import React from 'react';
import Example from 'views/Example';

const ExamplePage = () => {
  return (
    <div>
      <Example />
    </div>
  );
}

export default ExamplePage;
