const path = require('path');

module.exports = {
  stories: [
    "../stories/**/*.stories.mdx",
    "../stories/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions"
  ],
  
  webpackFinal: async (config, { configType }) => {

    config.resolve.modules = [path.resolve(__dirname, ".."), "node_modules"];

    config.resolve.alias = {
      ...config.resolve.alias,
      "@/section/*": path.resolve(__dirname, "components/Sections/*"),
      "@/ui/*": path.resolve(__dirname, "components/UI/*"),
      "@/layout/*": path.resolve(__dirname, "components/Layout/*"),
      "@/styles/*":  path.resolve(__dirname, "styles/*"),
      "@/utils/*": path.resolve(__dirname, "utils/*"),
      "@/images/*": path.resolve(__dirname, "assets/images/*"),
      "@": path.resolve(__dirname, "."),
    };

    return config;
  },
  framework: "@storybook/react",
  core: {
    "builder": "@storybook/builder-webpack5"
  },
}