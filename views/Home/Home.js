import styles from "./Home.module.scss";
import Layout from "@/layout/index";
import { WhatsNew, KnowUs, HeroTopSlider, HeroBottom } from "@/section/index";

export default function Home(props) {
  const meta = {
    title: "home",
    description: "coba description",
  };

  return (
    <Layout meta={meta}>
      <div className={styles.container}>
        <main className={styles.main}>
          <HeroTopSlider />
          <WhatsNew />
          <HeroBottom />
          <KnowUs />
        </main>
      </div>
    </Layout>
  );
}
