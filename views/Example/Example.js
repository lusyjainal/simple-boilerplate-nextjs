import React from 'react';
import Herotop from '@/section/HeroTop/HeroTop';
import Layout from '@/layout/index';
import WhatsNew from '@/section/WhatsNew/WhatsNew';

const Example = () => {
  const meta = {
    title: 'example',
    description: 'coba description'
  }

  return (
    <Layout meta={meta}>
      <Herotop />
      <WhatsNew />
    </Layout>
  );
}

export default Example;
