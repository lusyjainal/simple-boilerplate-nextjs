import React from 'react';
import Layout from "@/layout/index";
import MarkdownText from '@/ui/MarkdownText';
import styles from './DetailNews.module.scss';
import { BASE_URL } from 'constants/services';
import { Container } from '@mui/material';
import { Text } from '@/ui/index';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import Image from "next/image";

const DetailNews = (props) => {
    const meta = {
        title: "detail news",
        description: "coba description",
      };

    const { dataDetail } = props;

    function backButton() {
        history.back()
    }

    return (
        <Layout meta={meta}>
            <Container>
                <div className={styles['detail-news']}>
                    <div className="pointer" onClick={backButton}>
                        <ArrowBackIosIcon/>
                        <span className='v-super font-bold'>Back</span>
                    </div>
                    <Text variant='title-hero' className={styles.title}>{dataDetail.attributes.title}</Text>
                    <div className={`${styles['content__tag']} font-bold`}>{dataDetail.attributes.category.data?.attributes.title}</div>
                    <small className={styles.date}>
                        <CalendarTodayIcon fontSize="small" className='mr-2'/>
                        <span className='v-super'>{dataDetail.attributes.publishedAt.slice(0,10)}</span>
                    </small>
                    <div className={styles.content}>
                        <div className={styles['content__help']} />
                        <img src={`${BASE_URL}${dataDetail.attributes.image.data.attributes.url}`} />
                    </div>
                    <div className={styles['content__desc']}>
                        <MarkdownText markdown={dataDetail.attributes.description} />
                    </div>
                </div>
            </Container>
        </Layout>
    );
}

export default DetailNews;
