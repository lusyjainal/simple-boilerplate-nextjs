import React from 'react';
import Layout from "@/layout/index";

const News = () => {
    const meta = {
        title: "News",
        description: "coba description",
    };

    return (
        <Layout meta={meta}>
            News
        </Layout>
    );
}

export default News;
