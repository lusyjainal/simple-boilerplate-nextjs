import React from 'react';
import Layout from "@/layout/index";
import { AboutDescription, OurValue, KnowUs, AboutUsHero } from '@/section/index';
import styles from './AboutUs.module.scss';


const AboutUs = () => {
    const meta = {
        title: "About us",
        description: "coba description",
    };

    return (
        <Layout meta={meta}>
            <div className={styles.main}>
                <AboutUsHero></AboutUsHero>
                <AboutDescription />
                <OurValue />
                <KnowUs />
            </div>
        </Layout>
    );
}

export default AboutUs;
