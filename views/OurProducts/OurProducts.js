import React from 'react';
import Layout from "@/layout/index";

const OurProducts = () => {
    const meta = {
        title: "Our Products",
        description: "coba description",
    };

    return (
        <Layout meta={meta}>
            Our Products
        </Layout>
    );
}

export default OurProducts;
